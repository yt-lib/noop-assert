// @ts-check

const assert = () => {
	// noop
}

assert.fail = (/** @type {string | Error} */ err) => {
	if ('string' === typeof err) throw new Error(err)
	else throw err
}
assert.ok = () => {
	// noop
}
assert.equal = () => {
	// noop
}
assert.notEqual = () => {
	// noop
}
assert.deepEqual = () => {
	// noop
}
assert.notDeepEqual = () => {
	// noop
}
assert.strictEqual = () => {
	// noop
}
assert.notStrictEqual = () => {
	// noop
}
assert.deepStrictEqual = () => {
	// noop
}
assert.notDeepStrictEqual = () => {
	// noop
}
assert.throws = () => {
	// noop
}
assert.doesNotThrow = () => {
	// noop
}
assert.ifError = () => {
	// noop
}
assert.rejects = async () => {
	await Promise.resolve()
}
assert.doesNotReject = async () => {
	await Promise.resolve()
}
assert.match = () => {
	// noop
}
assert.doesNotMatch = () => {
	// noop
}

/** @type {import('assert')} */
module.exports = assert
